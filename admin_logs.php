<?php include('./include/navbar.php');
$currentpage = "admin_logs.php";
include('./backend/DatabaseConnect/DatabaseConnect.php');//Connect to the Database

$user_id = $_SESSION["user"];

$req = "SELECT c.id, c.deb_sess, c.fin_sess, c.id_user, u.username FROM connect c
JOIN user u ON c.id_user = u.id
ORDER BY deb_sess DESC;";

$resultat = mysqli_query($con, $req);
?>

<div class="container">
    <a class="btn btn-outline-primary" role="button" href="./admin_admin.php" style="margin-top: 20px">Revenir au panneau admin</a>
    <h1 class="text-center">Journal des connexions</h1>
    <hr>

    <table class="table table-hover table-bordered table-striped" style="margin: 20px 0 80px 0;">
        <tr>
            <th>Numéro</th>
            <th>Début de séssion</th>
            <th>Fin de séssion</th>
            <th>Pseudo</th>
            <th>Id utilisateur</th>
        </tr>
        <?php while ($ligne = mysqli_fetch_assoc($resultat)) { ?>
            <tr>
                <td> <?php echo $ligne['id']; ?> </td>
                <td> <?php echo $ligne['deb_sess']; ?> </td>
                <td> <?php echo $ligne['fin_sess']; ?> </td>
                <td> <?php echo $ligne['username']; ?> </td>
                <td> <?php echo $ligne['id_user']; ?> </td>
            </tr>
            <?php
        }
        ?>
    </table>
</div>

<?php
//AFFICHE LES ERREURS SQL
if (!$resultat) {
    echo mysqli_error($con);
}
?>

<?php
include('./include/footer.php');
?>
