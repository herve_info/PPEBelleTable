<?php include('./include/navbar.php');
$currentpage = "admin_commande_log.php";
include('./backend/DatabaseConnect/DatabaseConnect.php');//Connect to the Database

// Si la perssone est connécté et c'est un admin: elle accéde a ce contenu
if (isset($_SESSION['user']) && $_SESSION['level'] == 1) {
    ?>

    <div class="container">
        <?php
        ///Récupération de tout les utilisateurs
        $req = "SELECT * FROM commande ORDER BY id";
        $resultat = mysqli_query($con, $req);


        //		POUR VOIR LES ERREURS
        if (!$resultat) {
            echo mysqli_error($con);
        }
        //		FIN AFFICHAGE ERREURS
        ?>
    <a class="btn btn-outline-primary" role="button" href="./admin_admin.php" style="margin-top: 20px">Revenir au panneau admin</a>
        <h1 class="text-center">Historique des commandes</h1>
        <hr>

        <table class="table table-hover table-bordered table-striped" style="margin: 20px 0 80px 0;">
            <tr>
                <th>Numéro</th>
                <th>Date de commande</th>
                <th>Date de livraison</th>
                <th>Prix total</th>
                <th>Id utilisateur</th>
            </tr>
            <?php while ($ligne = mysqli_fetch_assoc($resultat)) { ?>
                <tr>
                    <td> <?php echo $ligne['id']; ?> </td>
                    <td> <?php echo $ligne['date_commande']; ?> </td>
                    <td> <?php echo $ligne['date_livraison']; ?> </td>
                    <td> <?php echo $ligne['prix_total']; ?> </td>
                    <td> <?php echo $ligne['id_user']; ?> </td>
                </tr>
                <?php
            }
            ?>
        </table>
    </div>
    <?php
} ///Si la perssone n'est pas admin on lui affiche ca:
else {
    ?>
    <h1>Page réservé aux administrateurs du site</h1>
    <?php
}
?>


<?php
include('./include/footer.php');
?>
