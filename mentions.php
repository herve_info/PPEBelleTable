<?php
$currentpage = "mentions.php";
include('include/navbar.php'); //permet d'inclure la navbar et le <head> en une ligne
?>

<div class="container">
    <h2 class="text-center" style="margin: 30px;">Mentions Légales</h2>
    <hr style="border-color: #2c2c54;">
    <section style="padding: 30px;">
        <p>Entreprise, organisme : Belle Table - SARL</p>
        <p>Capital social : 100 000€</p>
        <p>TVA N° : FR58 732 829 320</p>
        <p>SIRET :732 829 320 00074</p>
        <p>Siège social : 20 rue de la gare 75100 Paris</p>
        <p>Tel: +33 1 75 02 77 14</p>
        <p>reservation@belletable.com</p>
        <p>Objectif du site : Promotion de l’établissement</p>
        <p>Directeur de la publication, représentant légal : M. Jérôme Mativon</p>
        <p>Responsable éditorial : M. Pierre BARTHOLI</p>
        <p>Web master, conception, révision, direction artistique : Hérvé-Info - EURL</p>
        <p>Photographie : Vincent Lambert De Cursay</p>
        <p>Hébergement Internet : SAS OVH</p>
        <p>Président de SAS OVH : Henryk KLABA</p>
    </section>
</div>

<?php
include('./include/footer.php'); //permet d'inclure le footer en une ligne
?>
