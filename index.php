<?php
$currentpage = "index.php";
include('include/navbar.php'); //permet d'inclure la navbar et le <head> en une ligne
?>


<!-- Page Content -->

<div>
    <img src="./img/header2.jpg" alt="header2" class="img-fluid">
</div>
<div class="container" id="resume">
    <div class="row">
        <section class="col-lg-12">
            <h4 class="text-center">Notre expertise à votre service</h4>
            <p class="text-center"> Nous vous acconpagnons tout au long de la réalisation de votre progés. Les clients
                sont aujourd’hui très variés et nombreux. Tous types de professionnels faisant appel à la restauration
                font appel de façon régulière à Belle Table.
                Il y a des restaurants qui souhaitent compléter ou renouveler la vaisselle, les couverts, voir la
                totalité de leur salle de restauration, mais aussi des traiteurs (achats et surtout locations), les
                organisateurs d’évènements, les associations, comités d’entreprise, principalement pour la location.
                C'est ainsi que Belle Table s'est agrandi et comprend aujourd'hui plus de 40 salariés. </p>
        </section>
    </div>
</div>
<div class="container-fluid" id="skills_wrap">
    <div class="container" id="skills">
        <h3 class="text-center">Nos atouts</h3>
        <div class="row text-center">
            <div class="col-sm-3 skills_item">
                <i class="far fa-lightbulb" style="font-size: 3em;"></i>
                <hr>
                <h5>Des idées créatives</h5>
                <p>Pour réaliser vos projets a la hauteur de vos esperances</p>
            </div>
            <div class="col-sm-3 skills_item">
                <i class="fab fa-fort-awesome-alt" style="font-size: 3em;"></i>
                <hr>
                <h5>Des événements personnalisés</h5>
                <p>Adapté à votre budget et vos envie</p>
            </div>
            <div class="col-sm-3 skills_item">
                <i class="fas fa-ruler-combined" style="font-size: 3em;"></i>
                <hr>
                <h5>Des propositions sur mesure</h5>
                <p>Grace à notre large catalogue</p>
            </div>
            <div class="col-sm-3 skills_item">
                <i class="far fa-handshake" style="font-size: 3em;"></i>
                <hr>
                <h5>Une équipe d'experts</h5>
                <p>Formé par leurs années d'expérience dans l'organisation d'évenement</p>
            </div>
        </div>
    </div>
</div>

<?php
include('include/footer.php'); //permet d'inclure le footer en une ligne
?>
