<?php include('./include/navbar.php');
$currentpage = "admin_adminUpdate.php";
include('./backend/DatabaseConnect/DatabaseConnect.php');//Connect to the Database
?>
<!-- CETTE PAGE EST DISPO SEULEMENT POUR LES ADMINS !! -->
<!-- Formulaire de mise a jour d'un utilisater avec remplissage des champs automatique-->


<!--Si c'est bien un admin connecté-->
<?php if (isset($_SESSION['user']) && $_SESSION['level'] == 1)
{
    ?>

    <div class="container">

        <h1>Modifier d'un utilisateur</h1>
        <hr>

        <?php
        $id_user = htmlspecialchars($_GET["id_user"]);

        $req = "SELECT username, last_name, first_name, email, level FROM user WHERE id='$id_user'";
        $resultat = mysqli_query($con, $req);
        $ligne = mysqli_fetch_assoc($resultat);
        ?>

        <form method="POST" action="./backend/admin/adminUpdate.php?id_user=<?= $id_user ?>">
            <div class="row">
                <label for="username">Username: </label>
                <input type="text" class="form-control" name="username" id="username"
                       value="<?php echo $ligne['username'] ?>">
            </div>

            <div class="row">
                <label for="nom">Nom: </label>
                <input type="text" class="form-control" name="last_name" id="nom"
                       value="<?php echo $ligne['last_name'] ?>">
            </div>

            <div class="row">
                <label for="prenom">Prenom: </label>
                <input type="text" class="form-control" name="first_name" id="prenom"
                       value="<?php echo $ligne['first_name'] ?>">
            </div>

            <div class="row">
                <label for="email">email: </label>
                <input type="email" class="form-control" name="email" id="email" value="<?php echo $ligne['email'] ?>">
            </div>

            <div class="row">
                <p>0 si normal</p>
                <p>1 si Admin</p>
                <label for="level">Admin: </label>
                <input type="number" class="form-control" name="level" id="level" value="<?php echo $ligne['level'] ?>">
            </div>

            <input class="btn btn-primary" type="submit" value="Modifier">
        </form>

    </div>

    <?php
}
///Si la perssone n'est pas un admin
else {
    ?>
    <h1>Page réservé aux administrateurs du site</h1>
    <?php
}
?>

<?php
include('./include/footer.php');
?>
