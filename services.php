<?php
$currentpage = "services.php";
include('include/navbar.php'); //permet d'inclure la navbar et le <head> en une ligne
?>

<div class="container" style="padding: 50px;">
    <div class="container" style="margin-bottom: 5rem;">
        <h1 class="text-center">Nos Services</h1>
    </div>
    <div class="container">
        <p>Les fêtes de la vie, mêmes les plus belles, peuvent être entachées par des soucis d'organisation. En effet,
            la création d'une belle fête est particulièrement prenante et demande beaucoup de temps.</p>
        <p>Avez-vous dans votre vie quotidienne le temps nécessaire à lui consacrer ? Avez-vous le temps d'assurer le
            suivi de l'organisation ? Avez-vous l'énergie pour imaginer et mettre en place des éléments créatifs
            originaux ? Avez-vous le lieu adéquat pour accueillir vos invités, notamment en fonction des saisons ? </p>
        <p>Libérez-vous de toutes ces contraintes fastidieuses, et confiez en toute sérénité l'organisation de vos
            évènements à l'équipe sérieuse et expérimentée de Journois Évènementiel. Vous pourrez ainsi profiter
            pleinement et simplement du plaisir de votre fête en toute tranquilité, et accueillir vos invités l'esprit
            libre et détendu.</p>
        <p>Notre équipe s'occupe de toute l’organisation, de la préparation et de la gestion de vos réceptions.</p>
        <p>Les prestations que nous vous proposons sont diverses : Fiançailles, Enterrement de vie de jeune fille,
            Enterrement de vie de garçon, Anniversaires de mariage, Soirée d’anniversaire, Pendaison de crémaillère,
            Repas festifs, Anniversaire d’enfant, Baptême, Communion… Et toutes réunions que vous souhaitez joyeuses et
            conviviales.</p>
        <p>Vous êtes sûrement tentés, et vous pouvez nous demander tous les devis que vous souhaitez, sans aucun
            engagement de votre part. Quelle que soit la fête, même si elle ne figure pas dans les propositions
            ci-dessous, nous sommes là pour vous aider à réaliser vos idées les plus originales. Nous analyserons votre
            demande et nous vous proposerons l'offre la mieux adaptée.</p>
    </div>
</div>
<hr style="border-color: #2c2c54;"><!--Barre de séparation avec un style appliqué-->
<div class="container-fluid" style="padding: 50px;">
    <div class="row">
        <!--Thumbnail = miniature-->
        <div class="col-md-4">
            <div class="thumbnail">
                <img src="./img/thumb1.jpg" alt="Dressage" style="width:100%">
                <div class="caption">
                    <p>Evenements de communication et de promotion</p>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="thumbnail">
                <img src="./img/thumb2.jpg" alt="Dressage" style="width:100%">
                <div class="caption">
                    <p>Evenement festif d'entreprise ou d'équipe</p>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="thumbnail">
                <img src="./img/thumb3.jpg" alt="Dressage" style="width:100%">
                <div class="caption">
                    <p>Fiançailles</p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="thumbnail">
                <img src="./img/thumb4.jpg" alt="Dressage" style="width:100%">
                <div class="caption">
                    <p>Mariage</p>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="thumbnail">
                <img src="./img/thumb5.jpg" alt="Dressage" style="width:100%">
                <div class="caption">
                    <p>Enterrement de vie de jeune fille</p>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="thumbnail">
                <img src="./img/thumb6.jpg" alt="Dressage" style="width:100%">
                <div class="caption">
                    <p>Enterrement de vie de garçon</p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="thumbnail">
                <img src="./img/thumb7.jpg" alt="Dressage" style="width:100%">
                <div class="caption">
                    <p>Anniversaire</p>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="thumbnail">
                <img src="./img/thumb8.jpg" alt="Dressage" style="width:100%">
                <div class="caption">
                    <p>Anniversaire de mariage</p>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="thumbnail">
                <img src="./img/thumb9.jpg" alt="Dressage" style="width:100%">
                <div class="caption">
                    <p>Repas festifs</p>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
include('include/footer.php'); //permet d'inclure le footer en une ligne
?>
