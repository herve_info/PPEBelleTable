<?php
$currentpage = "presentation.php";
include('include/navbar.php'); //permet d'inclure la navbar et le <head> en une ligne
?>

<!-- PRESENTATION-CAROUSEL -->
<div class="container-fluid"><!--Permet d'occuper toute la largeur du viewport-->
    <div class="row">
        <!--Module Carousel-->
        <div id="carouselPresentation" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active carousel-item-presentation">
                    <img class="d-block w-100" src="./img/header.jpg" alt="image 1">
                </div>
                <div class="carousel-item carousel-item-presentation">
                    <img class="d-block w-100" src="./img/banner.jpg" alt="image 2">
                </div>
                <div class="carousel-item carousel-item-presentation">
                    <img class="d-block w-100" src="./img/header.jpg" alt="image 3">
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselPresentation" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselPresentation" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</div>

<!-- PRESENTATION-HISTORIQUE -->
<div class='container' id="container_historique">
    <div class='cotainer' id="historique">
        <h2 class="text-center">Notre histoire</h2>
        <p class="text-center">Belle Table est une entreprise créée par deux frères passionnés des arts de la table.
            Notre nom est représentatif de note activité. <br>Nous étions à la base une petite entreprise familiale de
            seulement 4 salariés. Notre entreprise a connu un franc succès et nos clients ont tous été satisfaits de nos
            prestations. <br>Les clients sont aujourd’hui très variés et nombreux. Tous types de professionnels faisant
            appel à la restauration font appel de façon régulière à Belle Table. <br>Il y a des restaurants qui
            souhaitent compléter ou renouveler la vaisselle, les couverts, voir la totalité de leur salle de
            restauration, mais aussi des traiteurs (achats et surtout locations), les organisateurs d’évènements, les
            associations, comités d’entreprise, principalement pour la location. C'est ainsi que Belle Table s'est
            agrandi et comprend aujourd'hui plus de 40 salariés. <br>Nous sommes là pour vous accompagner à dresser
            votre "Belle Table" ; notre objectif est la vente et la location de tous les matériels, la vaisselle, les
            couverts, les tables et chaises. Nous vous proposons aussi un service de qualité inclulant la livraison,
            mise en place et installation de tous les matériels. Aucune autre société ne propose un service aussi
            complet et haut de gamme avec des articles de la table de luxe..</p>
    </div>
</div>

<!-- PRESENTATION SKILLS -->
<div class="container-fluid" id="skills_wrap">
    <div class="container" id="skills">
        <h3 class="text-center">Nos atouts</h3>
        <div class="row text-center">
            <div class="col-sm-3 skills_item">
                <i class="far fa-lightbulb" style="font-size: 3em;"></i>
                <hr>
                <h5>Des idées créatives</h5>
                <p>Pour réaliser vos projets a la hauteur de vos esperances</p>
            </div>
            <div class="col-sm-3 skills_item">
                <i class="fab fa-fort-awesome-alt" style="font-size: 3em;"></i>
                <hr>
                <h5>Des événements personnalisés</h5>
                <p>Adapté à votre budget et vos envie</p>
            </div>
            <div class="col-sm-3 skills_item">
                <i class="fas fa-ruler-combined" style="font-size: 3em;"></i>
                <hr>
                <h5>Des propositions sur mesure</h5>
                <p>Grace à notre large catalogue</p>
            </div>
            <div class="col-sm-3 skills_item">
                <i class="far fa-handshake" style="font-size: 3em;"></i>
                <hr>
                <h5>Une équipe d'experts</h5>
                <p>Formé par leurs années d'expérience dans l'organisation d'évenement</p>
            </div>
        </div>
    </div>
</div>

<!-- PRESENTATION-PRESTA -->
<div class="container-fluid" id="container_pres_presta">
    <h3 class="text-center">Nos derniers dressages</h3>
    <div class="row">
        <div class="col-md-4">
            <div class="thumbnail">
                <img src="./img/header.jpg" alt="Dressage" style="width:100%">
            </div>
        </div>
        <div class="col-md-4">
            <div class="thumbnail">
                <img src="./img/header.jpg" alt="Dressage" style="width:100%">
            </div>
        </div>
        <div class="col-md-4">
            <div class="thumbnail">
                <img src="./img/header.jpg" alt="Dressage" style="width:100%">
                </a>
            </div>
        </div>
    </div>

</div>

<?php
include('include/footer.php'); //permet d'inclure le footer en une ligne
?>
