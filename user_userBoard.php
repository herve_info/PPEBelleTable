<?php
$currentpage = "user_userBoard.php";
include('./include/navbar.php');//permet d'inclure la navbar et le <head> en une ligne

include('./backend/DatabaseConnect/DatabaseConnect.php');//Connect to the Database

$user_id = htmlspecialchars($_SESSION['user']);

//SELECT ALL USERS FOR PROFILE MODIFICATION
$reqUsers = "SELECT * FROM user WHERE id='$user_id'";
$resUsers = mysqli_query($con, $reqUsers);
$users = mysqli_fetch_assoc($resUsers);


//SELECT ALL AVIS FOR SHOW AVIS TAB
$reqAvis = "SELECT * FROM avis WHERE id_user='$user_id'";
$resAvis = mysqli_query($con, $reqAvis);
?>

<div class="container">
    <nav>
        <div class="nav nav-tabs" id="nav-tab" role="tablist">
            <a class="nav-item nav-link active" id="nav-profile-tab" data-toggle="tab" href="#nav-profile"
               role="tab"
               aria-controls="nav-profile" aria-selected="true">Mon profil</a>
            <a class="nav-item nav-link" id="nav-avis-tab" data-toggle="tab" href="#nav-avis" role="tab"
               aria-controls="nav-avis" aria-selected="false">Mes avis</a>
        </div>
    </nav>


    <!-- Modification profil tab -->
    <div class="container tab-content" id="nav-tabContent">
        <div class="tab-pane fade show active" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
            <div class="container">
                <h1 class="text-center">Modifier votre profil</h1>
                <hr>

                <form class="container" method="post" action="./backend/user/userUpdate.php">
                    <div class="form-row">
                        <div class="form-group col-6">
                            <label for="username">Username</label>
                            <input type="text" class="form-control" name="username" id="username"
                                   value="<?= $users['username'] ?>">
                        </div>
                        <div class="form-group col-6">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" name="password" id="password"
                                   value="<?= $users['password'] ?>">
                        </div>

                        <!-- Level du compte en hidden, seul l'admin peut changer l'attribut -->
                        <input type="hidden" class="form-control" name="level" id="level"
                               value="<?= $users['level'] ?>">

                        <div class="form-group col-md-12">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" name="email" id="email"
                                   value="<?= $users['email'] ?>">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="nom">Nom</label>
                            <input type="text" class="form-control" name="last_name" id="nom"
                                   value="<?= $users['last_name'] ?>">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="prenom">Prénom</label>
                            <input type="text" class="form-control" name="first_name" id="prenom"
                                   value="<?= $users['first_name'] ?>">
                        </div>
                        <div class="form-group col-md-12">
                            <label for="adress">Address</label>
                            <input type="text" class="form-control" name="adresse" id="adress"
                                   value="<?= $users['adresse'] ?>">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="ville">Ville</label>
                            <input type="text" class="form-control" name="ville" id="ville"
                                   value="<?= $users['ville'] ?>">
                        </div>
                        <div class="form-group col-md-2">
                            <label for="cp">Code postal</label>
                            <input type="text" class="form-control" name="code_postal" id="cp"
                                   value="<?= $users['code_postal'] ?>">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="tel">Numéro de telephone</label>
                            <input type="tel" class="form-control" name="telephone" id="tel"
                                   value="<?= $users['telephone'] ?>">
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary btn-lg btn-block">Modifier</button>
                </form>
            </div>
        </div>


        <!-- Historique avis posté tab -->
        <div class="container tab-pane fade" id="nav-avis" role="tabpanel" aria-labelledby="nav-avis-tab">
            <div class="container">
                <h1 class="text-center">Historique des avis postés</h1>
                <hr>

                <table class="table table-hover table-bordered table-striped" style="margin: 20px 0 80px 0;">
                    <tr>
                        <th class="text-center">Titre</th>
                        <th class="text-center">Date</th>
                        <th class="text-center">Avis</th>
                    </tr>
                    <?php while ($avis = mysqli_fetch_assoc($resAvis)) { ?>
                        <tr>
                            <td> <?php echo $avis['titre']; ?> </td>
                            <td> <?php echo $avis['date']; ?> </td>
                            <td> <?php echo $avis['avis']; ?> </td>
                        </tr>
                        <?php
                    }
                    ?>
                </table>
            </div>
        </div>
    </div>
</div>


<?php
include('./include/footer.php');
?>
