<?php
$currentpage = "avis.php";
include('include/navbar.php');//permet d'inclure la navbar et le <head> en une ligne

include('./backend/DatabaseConnect/DatabaseConnect.php');//Connect to the Database

//Récupère les avis et l'username de celui qui l'a posté
$reqAvis = "SELECT username, titre, date, avis FROM user u, avis
WHERE u.id IN
(SELECT id_user FROM avis);";
$resAvis = mysqli_query($con, $reqAvis);
?>

<!-- Page Content -->
<div class="container " id="container.Custom-NoMargin">
    <div class="row">
        <div id="carouselAvis" class="carousel slide" data-ride="carousel" class="col-lg-12">
            <div class="carousel-inner">
                <?php
                $numSlide = 0;
                while ($avis = mysqli_fetch_assoc($resAvis)) {
// Pour pallier au problème d'affiche des carousel-item. Une seul item peut avoir l'attribut active.
                    $numSlide += 1;
                    if ($numSlide == 1)
                        $active = "active";
                    else
                        $active = "";
                    ?>
                    <div class="carousel-item <?= $active ?> carousel-item-avis">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-1">
                                </div>
                                <div class="col-lg-2">
                                    ★★★
                                </div>
                                <div class="col-lg-4">
                                    <h5>Titre</h5>
                                    <?= $avis['titre'] ?>
                                </div>
                                <div class="col-lg-2">
                                    <h5>Pseudo</h5>
                                    <?= $avis['username'] ?>
                                </div>
                                <div class="col-lg-2">
                                    <h5>Date</h5>
                                    <?= $date = date_format(date_create_from_format('Y-m-d', $avis['date']), 'd.m.Y'); ?>
                                </div>
                                <div class="col-lg-1">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-1">
                                </div>
                                <div class="col-lg-10">
                                    <h5>Avis</h5>
                                    <?= $avis['avis'] ?>
                                </div>
                                <div class="col-lg-1">
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>

            <a class="carousel-control-prev" href="#carouselAvis" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselAvis" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>


    <div class="card card-avis">
        <form method="post" action="/PPEBelleTable/core/backend/avis.php">
            <div class="card-body">
                <h2 class="card-title">Votre avis</h2>
                <div class="card-text">
                    <div class="row">
                        <div class="rating col-lg-3">
                            <input type="radio" id="star10" name="rating" value="10"/><label for="star10"
                                                                                             title="Rocks!">5
                                stars</label>
                            <input type="radio" id="star9" name="rating" value="9"/><label for="star9" title="Rocks!">4
                                stars</label>
                            <input type="radio" id="star8" name="rating" value="8"/><label for="star8"
                                                                                           title="Pretty good">3
                                stars</label>
                            <input type="radio" id="star7" name="rating" value="7"/><label for="star7"
                                                                                           title="Pretty good">2
                                stars</label>
                            <input type="radio" id="star6" name="rating" value="6"/><label for="star6" title="Meh">1
                                star</label>
                            <input type="radio" id="star5" name="rating" value="5"/><label for="star5" title="Meh">5
                                stars</label>
                            <input type="radio" id="star4" name="rating" value="4"/><label for="star4"
                                                                                           title="Kinda bad">4
                                stars</label>
                            <input type="radio" id="star3" name="rating" value="3"/><label for="star3"
                                                                                           title="Kinda bad">3
                                stars</label>
                            <input type="radio" id="star2" name="rating" value="2"/><label for="star2"
                                                                                           title="Sucks big tim">2
                                stars</label>
                            <input type="radio" id="star1" name="rating" value="1"/><label for="star1"
                                                                                           title="Sucks big time">1
                                star</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-12">
                            <input class="form-control" type="text" placeholder="Titre de votre avis" name="TitreAvis">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-lg-12">
                            <textarea class="form-control" placeholder="Votre avis" rows="8"
                                      name="TextAvis"></textarea>
                        </div>
                    </div>
                </div>

                <?php
                if (isset($_SESSION['user']) && !empty($_SESSION['user'])) {
                    ?>
                    <input type="submit" name="btn_avis_envoi" value="Envoyer" class="btn btn-primary centered">
                    <?php
                } else { ?>
                    <input type="button" value="Envoyer" class="btn btn-secondary centered ">
                    <div style="color:Tomato;">Veuillez vous connecter pour poster un avis</div>
                    <?php
                } ?>
            </div>
        </form>
    </div>
</div>

<?php
include('include/footer.php');//permet d'inclure le footer en une ligne
?>
