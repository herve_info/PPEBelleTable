<?php include('./include/navbar.php');
$currentpage = "admin_admin.php";
include('./backend/DatabaseConnect/DatabaseConnect.php');//Connect to the Database

// Si la perssone est connécté et c'est un admin: elle accéde a ce contenu
if (isset($_SESSION['user']) && $_SESSION['level'] == 1) {
    ?>

    <div class="container">
        <h1 class="text-center">Liste des utilisateurs</h1>
        <hr>

        <?php
        ///Récupération de tout les utilisateurs
        $req = "SELECT * FROM user ORDER BY id";
        $resultat = mysqli_query($con, $req);


        //		POUR VOIR LES ERREURS
        if (!$resultat) {
            echo mysqli_error($con);
        }
        //		FIN AFFICHAGE ERREURS
        ?>
        <!-- Bouton pour accéder a la page adminAdd qui permet d'ajouter un utilisateur-->
        <div class="row">
            <div style="margin-bottom: 30px;">
                <a href="./admin_adminAdd.php" class="btn btn-default">Ajouter un
                    utilisateur</a>
            </div>
            <div style="margin-bottom: 30px;">
                <a href="./admin_logs.php" class="btn btn-default">Journal des logs</a>
            </div>
            <div style="margin-bottom: 30px;">
                <a href="./admin_commande_log.php" class="btn btn-default">Historique des commandes</a>
            </div>
        </div>

        <div class="row">

            <!-- Affichage de tout les utilisateurs dans un tableau -->
            <table class="table table-hover table-bordered table-striped" style="margin: 50px 0 80px 0;">
                <tr>
                    <th>Id</th>
                    <th>Pseudo</th>
                    <th>Nom</th>
                    <th>Prenom</th>
                    <th>email</th>
                    <th style="width:10px">MODIFIER</th>
                    <th style="width:10px">SUPPRIMER</th>
                </tr>
                <?php while ($ligne = mysqli_fetch_assoc($resultat)) { ?>
                    <tr>
                        <td> <?php echo $ligne['id']; ?> </td>
                        <td> <?php echo $ligne['username']; ?> </td>
                        <td> <?php echo $ligne['last_name']; ?> </td>
                        <td> <?php echo $ligne['first_name']; ?> </td>
                        <td> <?php echo $ligne['email']; ?> </td>
                        <td>
                            <a href="./admin_adminUpdate.php?id_user= <?= $ligne['id']; ?> ">X</span>
                        </td>
                        <td>
                            <a href="./backend/admin/adminDelete.php?id_user= <?= $ligne['id']; ?> ">X</span>
                        </td>
                    </tr>
                    <?php
                }
                ?>
            </table>
        </div>
    </div>

    <?php
} ///Si la perssone n'est pas admin on lui affiche ca:
else {
    ?>
    <h1>Page réservé aux administrateurs du site</h1>
    <?php
}
?>


<?php
include('./include/footer.php');
?>
