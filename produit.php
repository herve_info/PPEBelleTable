<?php
$currentpage = "produit.php";
include('include/navbar.php'); //permet d'inclure la navbar et le <head> en une ligne

include './backend/DatabaseConnect/DatabaseConnect.php';//Connect to the database

if (empty($_GET["categorie"])) {
    $req = "SELECT * FROM `produit`"; //Get all the playlists
} else {
    $req = "SELECT * FROM `produit` WHERE id_categorie = " . $_GET['categorie'];
}


$ProductTable = mysqli_query($con, $req); //result of the request
?>

<div class="container" style="padding: 50px;">
    <div class="row">
        <a href="produit.php" type="button" class="btn btn-secondary col" id="button-product">Tout</a>
        <a href="produit.php?categorie=1" type="button" class="btn btn-secondary col" id="button-product">Assiette</a>
        <a href="produit.php?categorie=2" type="button" class="btn btn-secondary col" id="button-product">Couvert</a>
        <a href="produit.php?categorie=3" type="button" class="btn btn-secondary col" id="button-product">Verrerie</a>
    </div>
    <div class="row">
        <?php
        for ($ProductNumber = 0;
             $ProductNumber < mysqli_num_rows($ProductTable);
             $ProductNumber++) {

            $LineProduct = mysqli_fetch_assoc($ProductTable);

            ?>
            <div class="card-center col-xs-12 col-sm-6 col-md-4 col-lg-3" style="padding-bottom: 1rem;">
                <div class="card">
                    <div class="card-body">
                        <img class="card-img-top" alt="photo product" src=".\img\product\<?php echo $LineProduct["id"]; ?>.jpg"/>
                        <p class="card-text"><?php echo $LineProduct["description"]; ?></p>
                        <p class="card-text"><?php echo $LineProduct["prix_unitaire"]; ?>€</p>
                        <button class="btn btn-info AddCartButton" id="AddCartButton<?php echo $LineProduct["id"] ?>">Ajouter au panier</button>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>

<?php
include('include/footer.php'); //permet d'inclure le footer en une ligne
?>
