<?php
$currentpage = "user_shoppingcart.php";
include('./include/navbar.php');//permet d'inclure la navbar et le <head> en une ligne

include('./backend/product/product.php');
?>

<?php

//https://bootsnipp.com/snippets/yP7qe
//https://www.withinweb.com/info/a-shopping-cart-using-php-sessions-code/

 //variable de Total
  $TotalValue = 0;
?>


<div class="container">
    <table id="cart" class="table table-hover table-condensed">
        <thead>
        <tr>
            <th style="width:50%">Produit</th>
            <th style="width:10%">Prix</th>
            <th style="width:8%">Quantité</th>
            <th style="width:22%" class="text-center">Sous total</th>
            <th style="width:10%"></th>
        </tr>
        </thead>
        <tbody>

<?php
if (!isset($_SESSION['shoppingcart'])) {
  $_SESSION['shoppingcart'] = array();
}

foreach ($_SESSION['shoppingcart'] as $key => $value) {

        $ProducInfo = GetProductInfo($key);

    ?>
        <tr>

            <td data-th="Product">
                <div class="row">
                    <div class="col-sm-2 hidden-xs"><img src=".\img\product\<?php echo $key ?>.jpg" class="img_tumbnail img-responsive" alt="..."/></div>
                    <div class="col-sm-10">
                        <h4 class="nomargin"><?php echo $ProducInfo['description']; ?></h4>
                    </div>
                </div>
            </td>
            <td data-th="Price" id="Price<?php echo $key ?>"><?php echo $ProducInfo['prix_unitaire']; ?>$</td>
            <td data-th="Quantity">
                <input type="number" class="form-control text-center QuantityInput" id="QuantityInput<?php echo $key ?>" value="<?php echo $_SESSION['shoppingcart'][$key] ?>">
            </td>
            <td data-th="Subtotal" class="text-center Subtotal" id="Subtotal<?php echo $key ?>"><?php $TotalValue +=  $ProducInfo['prix_unitaire']* $_SESSION['shoppingcart'][$key]; echo $ProducInfo['prix_unitaire']* $_SESSION['shoppingcart'][$key]; ?>$</td>
            <td class="actions" data-th="">
                <button class="btn btn-info btn-sm"><i class="fa fa-refresh"></i></button>
                <button class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button>
            </td>

        </tr>

        <?php
        }?>


        </tbody>
        <tfoot>
        <tr>
            <td><a href="./produit.php" class="btn btn-warning"><i class="fa fa-angle-left"></i> Retourner vers les produits</a></td>
            <td colspan="2" class="hidden-xs"></td>
            <td class="hidden-xs text-center" id="Total"><strong>Total: <?php echo $TotalValue; ?>$</strong></td>
            <form class="" action="./backend/product/commande.php" method="post">

              <td>  <input type="submit" type="button" class="btn btn-primary" name="CommandeButton" value="Commander"></td>

            </form>

        </tr>
        </tfoot>
    </table>
</div>

<?php
include('./include/footer.php'); //permet d'inclure le footer en une ligne
?>
