<?php session_start();

//if the disconnect from the navbar is clicked and the user have a session
if (isset($_POST["DisconnectButton"]) && !empty($_SESSION['user'])) {

    //Save the end of the session on connection logs
    include('../admin/update_log.php'); //save the log of the connection
    //erase complet user session
    session_destroy();

    //current page refere to the name of the page in the frontend folder
    $currentpage = $_POST["Currentpage"];

    //if you disconnect from an admin or user's page, it will redirect to index (using regex)
    if (preg_match("#admin#", $currentpage)) {
        header("Location: ../../");
    } elseif (preg_match("#user#", $currentpage)) {
        header("Location: ../../");
    } else {
        header("Location: ../../$currentpage");
    }
}else {
    header("Location: ../../$currentpage");
}
