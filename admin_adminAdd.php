<?php include('./include/navbar.php');
$currentpage = "admin_adminAdd.php";
?>

<!-- Formulaire d'ajout d'utilisater par un admin-->

<!-- Si c'est bien un admin:-->
<?php if (isset($_SESSION['user']) && $_SESSION['level'] == 1)
{
    ?>

<div class="container">
    <a class="btn btn-outline-primary" role="button" href="./admin_admin.php" style="margin-top: 20px">Revenir au panneau admin</a>
    <h1 class="text-center">Ajout</h1>
    <hr>

    <form class="container" method="post" action="./backend/admin/adminAdd.php">
        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="username">Username</label>
                <input type="text" class="form-control" name="username" id="username" placeholder="Username">
            </div>
            <div class="form-group col-md-5">
                <label for="password">Password</label>
                <input type="password" class="form-control" name="password" id="password" placeholder="Password">
            </div>
            <div class="form-group col-md-3">
                <label for="level">Compte admin</label>
                <input type="number" class="form-control" name="level" id="level" placeholder="0 ou 1">
            </div>
            <div class="form-group col-md-12">
                <label for="email">Email</label>
                <input type="email" class="form-control" name="email" id="email" placeholder="Email">
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="nom">Nom</label>
                <input type="text" class="form-control" name="last_name" id="nom">
            </div>
            <div class="form-group col-md-6">
                <label for="prenom">Prénom</label>
                <input type="text" class="form-control" name="first_name" id="prenom">
            </div>
            <div class="form-group col-md-12">
                <label for="adress">Address</label>
                <input type="text" class="form-control" name="adresse" id="adress" placeholder="2 rue de Paris">
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="ville">Ville</label>
                <input type="text" class="form-control" name="ville" id="ville">
            </div>
            <div class="form-group col-md-2">
                <label for="cp">Code postal</label>
                <input type="text" class="form-control" name="code_postal" id="cp">
            </div>
            <div class="form-group col-md-4">
                <label for="tel">Numéro de telephone</label>
                <input type="tel" class="form-control" name="telephone" id="tel">
            </div>
        </div>
        <button type="submit" class="btn btn-primary btn-lg btn-block">Inscription</button>
    </form>
</div>



    <?php
}
///si ce n'est pas un admin
else {
    ?>
    <h1>Page réservé aux administrateurs du site</h1>
    <?php
}
?>

<?php
include('./include/footer.php');
?>
