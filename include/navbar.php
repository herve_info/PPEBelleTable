<?php session_start();
$currentpage = "index.php";
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Site vitrine de Belle Table">
    <meta name="author" content="Hérvé-Info">
    <link rel="icon" href="./img/favicon.ico"/>

    <title>Belle table</title>

    <!-- Bootstrap core CSS -->
    <link href="./vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="./style/style.css" rel="stylesheet">


    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Great+Vibes" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

    <!-- FontAwesome Icons -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css"
          integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">

    <!-- Custom styles for the navbar -->
    <style>
        body {
            padding-top: 65px;
        }

        @media (min-width: 992px) {
            body {
                padding-top: 65px;
            }
        }
    </style>
</head>
<body>


<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="./index.php">Belle Table</a>
        <!--NavBar responsive-->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="./presentation.php">Présentation
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="./services.php">Services</a>
                    <span class="sr-only">(current)</span>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="./produit.php">Produits</a>
                    <span class="sr-only">(current)</span>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="./user_avis.php">Avis</a>
                    <span class="sr-only">(current)</span>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="./contact.php">Contact</a>
                    <span class="sr-only">(current)</span>
                </li>
                <li class="nav-item navbar-marge-button">
                    <a type="button" class="btn"
                       href="./user_shoppingcart.php">
                        <img class="img_shoppingcart" src="./img/shopping-cart.png" alt="">
                    </a>
                </li>

                <!-- Affichage bouton "Admin" si user admin connecte -->
                <?php
                if (isset($_SESSION['user']) && $_SESSION['level'] == 1) {
                    ?>

                    <li class="nav-item navbar-marge-button">
                        <a type="button" class="btn btn-primary"
                           href="./admin_admin.php">
                            Admin
                        </a>
                    </li>

                <?php } elseif (isset($_SESSION['user']) && $_SESSION['level'] != 1) {
                    ?>
                    <li class="nav-item navbar-marge-button">
                        <a type="button" class="btn btn-primary"
                           href="./user_userBoard.php">
                            User
                        </a>
                    </li>
                    <?php
                }
                ?>


                <!-- Affichage bouton "Déconnexion" si user connecte -->
                <?php
                if (isset($_SESSION['user']) && !empty($_SESSION['user'])) {
                    ?>
                    <form action="./backend/user/Disconnect.php" method="post">

                        <input type="hidden" name="Currentpage" value="<?php echo $currentpage; ?>">

                        <li class="nav-item navbar-marge-button">
                            <input type="submit" type="button" class="btn btn-primary" name="DisconnectButton"
                                   value="Déconnexion">
                        </li>

                    </form>


                    <!-- Affichage boutons "Connexion" & "inscription" si pas de user connecte -->
                    <?php
                } else {
                    ?>

                    <li class="nav-item navbar-marge-button">
                        <!-- Sign in Button trigger modal -->
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#SigninModal">
                            Connexion
                        </button>
                    </li>
                    <li class="nav-item  active navbar-marge-button">
                        <!-- Sign up Button trigger modal -->
                        <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#SignupModal">
                            S'inscrire
                        </button>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>
</nav>


<!-- Sign in Modal -->
<div class="modal fade" id="SigninModal" tabindex="-1" role="dialog" aria-labelledby="SigninModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="SignupModalLabel">Connexion</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="./backend/user/SignIn.php" method="post">
                    <input type="text" class="form-control" placeholder="Identifiant" name="SignInUsername" required>
                    <br>
                    <input type="password" class="form-control" placeholder="Mot de passe" name="SignInPassword"
                           required>
                    <input type="hidden" name="Currentpage" value="<?php echo $currentpage; ?>">
                    <div class="modal-footer">
                        <input type="submit" type="button" class="btn btn-primary" name="SignInButton" value="Envoyer">
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
<!-- Sign up Modal -->
<div class="modal fade" id="SignupModal" tabindex="-1" role="dialog" aria-labelledby="SignupModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="SignupModalLabel">Inscription</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="./backend/user/SignUp.php" method="post">
                    <input type="text" class="form-control" placeholder="Identifiant" name="SignUpUsername" required>
                    <br>
                    <input type="text" class="form-control" placeholder="Nom" name="SignUpLast_name" required> <br>
                    <input type="text" class="form-control" placeholder="Prenom" name="SignUpFirst_name" required> <br>
                    <input type="email" class="form-control" placeholder="Email" name="SignUpEmail" required> <br>
                    <input type="text" class="form-control" placeholder="Adresse" name="SignUpAdress" required> <br>
                    <input type="password" class="form-control" placeholder="Mot de passe" name="SignUpPassword"
                           required> <br>
                    <input type="phone" class="form-control" placeholder="Telephone" name="SignUpPhone" required> <br>
                    <input type="password" class="form-control" placeholder="Confirmation Mot de passe"
                           name="password_confirmation" required> <br>

                    <input type="hidden" name="Currentpage" value="<?php echo $currentpage; ?>">

                    <div class="modal-footer">
                        <input type="submit" type="button" class="btn btn-primary" name="SignUpButton" value="Envoyer">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
