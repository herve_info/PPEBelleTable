
<div class="card-footer footer">
    <div class="row text-center" id="footer_inner">
        <div class="col-lg-4 footer_item">
            <ul>
                <li><a href="./services.php" alt="Services">Nos Services</a></li>
                <li><a href="#" alt="Remboursement">Modalités de remboursement</a></li>
                <li><a href="#" alt="CGV">CGV</a></li>
            </ul>
        </div>
        <div class="col-lg-4 footer_item">
            <ul>
                <li><a href="./mentions.php" alt="Mentions légales">Mentions légales</a></li>
                <li><a href="#" alt="Partenariat">Partenariat</a></li>
                <li><a href="#" alt="Politiques de cookies">Politiques de cookies</a></li>
                <br>
                <a href="http://www.facebook.com" target="_blank"><i class="fab fa-facebook fab_footer" style="font-size:36px;"></i></a>
                <a href="http://www.pinterest.com" target="_blank"><i class="fab fa-pinterest fab_footer" style="font-size:36px;"></i></a>
                <a href="http://www.instagram.com" target="_blank"><i class="fab fa-instagram fab_footer" style="font-size:36px;"></i></a>
            </ul>
        </div>
        <div class="col-lg-4 footer_item">
            <ul>
                <li><a href="#" alt="Plan du site">Plan du site</a></li>
                <li><a href="#" alt="Recrutement">Recrutement</a></li>
                <li><a href="./avis.php" alt="Avis">Avis Belle Table</a></li>

            </ul>
        </div>
    </div>
    <div class='text-center' id="copynote"><p>© Hérvé Info 2019. Tous droits réservés. </p></div>
</div>



<!-- Bootstrap core JavaScript -->
<script src="./vendor/jquery/jquery.min.js"></script>
<script src="./vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Script Jquery -->
<script type="text/javascript" src="./js/function.js"></script>

</body>

</html>
