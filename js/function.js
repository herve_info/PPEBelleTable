jQuery(document).ready(function ($) {




///PAGE PRODUIT et PANIER

    //bouton ajout panier
    $(".AddCartButton").click(function () {

      //$('.AddCartButton#AddCartButton' + this.id.slice(13)).removeClass("disabled IconRed");

      //https://codepen.io/elmahdim/pen/tEeDn animation image vers panier

      //fait une requéte  a la page qui ajoute le produit dans le panier
      $.ajax({

        url: './backend/user/shoppingcartfunction.php',

        type: 'GET',

        data: 'productid=' + this.id.slice(13)

      });

    });

    //Change la valeur du sous-total au changement de la quantité du panier
    $(".QuantityInput").on("change paste keyup", function() {

        //Met a jour le sous total
        var ProductId = this.id.slice(13);
        $("#Subtotal" + ProductId).html( (parseInt($(this).val()) * parseInt($("#Price" + ProductId).html())) + "$");

    });

    //Met a jour la valeur du total du panier
    $("body").on('DOMSubtreeModified', ".Subtotal", function() {

      var Total = 0;
      $(".Subtotal").each(function() {
          Total += parseInt($( this ).html().slice(0,-1));
        });
      $("#Total").html( "<strong> Total: " + Total + "$ </strong>");

    });


    //Quand l'utilisateur apuis sur le bouton Commander
    $(".GoodPointVideoButton").click(function () {

    $.ajax({

      url: '../backendphp/shoppingcartCommander.php', // La ressource ciblée

      type: 'GET', // Le type de la requête HTTP.

      data: 'commander=1'

    });

  });



});
