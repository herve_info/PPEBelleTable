<?php
$currentpage = "contact.php";
include('include/navbar.php'); //permet d'inclure la navbar et le <head> en une ligne
?>


<div class="container">
    <div class="card">
        <form method="post" action="/PPEBelleTable/core/backend/contact.php">
            <div class="card-body">
                <h1 class="card-title">Nous contacter</h1>
                <p class="card-text ">
                <div class="row">
                    <div class="col-md-6 form-group">
                        <input type="text" class="form-control" placeholder="Nom" name="NomContact">
                    </div>
                    <div class="col-md-6 form-group">
                        <input type="text" class="form-control" placeholder="Prénom" name="PrenomContact">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 form-group">
                        <input type="email" class="form-control" placeholder="Adresse mail" name="emailContact">
                    </div>
                    <div class="col-md-6">
                        <input type="text" class="form-control" placeholder="Entreprise" name="EntrepriseContact">
                    </div>
                </div>
                <div class="row">
                  <div class="col-md-12 form-group">
                      <input type="text" class="form-control" placeholder="Sujet" name="SujetContact">
                  </div>
                </div>
                <div class="row">
                    <div class="form-group col-12">
                        <textarea class="form-control" placeholder="Message" name="messageContact" rows="6"></textarea>
                    </div>
                </div>
                </p>
                <div class="form-group">
                    <input type="submit" name="btn_contact_envoi" value="Envoyer" class="btn btn-primary centered">
                </div>
            </div>
        </form>
    </div>
</div>

<?php
include('include/footer.php'); //permet d'inclure le footer en une ligne
?>
